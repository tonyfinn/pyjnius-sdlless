# on android, rely on SDL to get the JNI env
cdef extern JNIEnv *PYDROID_GetJNIEnv()

cdef JNIEnv *get_platform_jnienv():
    return PYDROID_GetJNIEnv()
